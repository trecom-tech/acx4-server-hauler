const { auditSchema, addressSchema, freightSchema, incotermType,
  freightServiceSchema, weightSchema, volumeSchema, transformPlugin  } = require('@axioncore/acx-server-core').model


// Options
let options = {timestamps: true}

module.exports = ({mongoose}) => {

  // Schema
  let schema = mongoose.Schema({
    // Main header fields
    accountId: { type: String, required: true },
    date: {type: Number, required: true},
    number: String,
    name: String,

    _search: {type: String, select: false},

    status: {
      type: String,
      default: 'PENDING',
      enum: ['PENDING', 'ASSIGNED', 'READY', 'LOADING', 'LOADED', 'IN_PROGRESS', 'COMPLETE', 'CANCELED'],
      required: true
    },

    // Reference to the original logisitc job
    customer: {
      accountId: String,
      name: String,
      job: {
        id: String,
        number: String
      }
    },

    // Assigned truck info
    truck: {
      id: String,
      name: String,
      plateNumber: String,
      capacity: {
        weight: weightSchema,
        volume: volumeSchema,
      }
    },

    // Assigned driver info
    driver: {
      id: String,
      fullName: String,
      phone: String
    },

    // Shipments / Legs included in this job
    shipments: [{
      // no artificial id
      _id: false,
      // Shipment info
      shipment: {
        id: {type: String, required: true},
        number: String,
        legId: {type: String, required: true}
      },

      // Original Shipment Request Info
      request: {
        id: String,
        name: String
      },

      // Sale Order info, optional
      order: {
        id: {type: String, required: false},
        number: String,
      },

      // Shipper info, optional for now
      shipper: {
        id: {type: String, required: false},
        name: String
      },

      // Consignee (receiver) details
      consignee: {
        name: String,
        contacts: [
          {
            name: String,
            email: String,
            phones: [{
              type: String,
              value: String
            }]
          }
        ]
      },

      // Freight info
      freight: freightSchema,

      services: freightServiceSchema,

      term: incotermType,

      expectedDeliverPeriod: {
        startDate: Number,
        endDate: Number
      },

      expectedPickupPeriod: {
        startDate: Number,
        endDate: Number,
      },

      // Status of Shipment
      status: {type: String, default: 'NOT_LOADED', enum:['NOT_LOADED', 'LOADED', 'DELIVERED', 'NOT_ACCEPTED', 'CANCELED']},

      // Note
      note: String,
    }],


    // Route points
    route: [{
      _id: false,
      // Unique internal point id
      // DO WE STILL NEED IT ?
      id: {type: String, required: false},

      // Sequence number
      sequence: {type: Number, required: true},

      // Point Type
      type: {type: String, required: true, enums: ['WAREHOUSE', 'CUSTOM']},

      // Warehouse
      // Keep for future, not used in Marketplace
      warehouse: {
        id: String,
        name: String
      },

      // Address
      address: addressSchema,

      note: String,

      // List of Shipment IDs handled (loaded or delivered) at this location
      shipments: [{
        _id: false,
        id: String,
        legId: String,
        number: String,
        type: {type: String, enum: ['LOAD', 'DELIVER']},
        // Contact info at this point/location
        //contacts: [personSchema],
        note: String,
        scheduledPeriod: {
          startDate: Number,
          endDate: Number
        }
      }],

      status: {type: String, required: true, default: 'PENDING', enum:['PENDING', 'IN_PROGRESS', 'ARRIVED', 'COMPLETE']}
    }],

    mapData: {
      source: {type: String, enum:['MAPBOX'] },
      route: [],
      distance: {
        value: Number,
        uom: String
      },
      time: {
        value: Number,
        uom: String
      }
    },

    // Change history
    audit: [{
      type: auditSchema
    }],
    active: {type: Boolean, required: true, default: true}
  }, options)

  // Pre-save hook to populate some fields
  schema.pre('save', function(next) {
    this._search = (
      this.number + ' ' + this.name +
      ( this.truck && this.truck.plateNumber ? ' ' + this.truck.plateNumber : '') +
      ( this.driver && this.driver.fullName ? ' ' + this.driver.fullName : '')
    ).toLowerCase()
    next()
  })

  // Add listView function
  schema.methods.listView = function() {
    let ret = this.toObject()
    delete ret.__v
    delete ret.shipments
    delete ret.audit
    return ret
  }


  // Indexes
  schema.index({ accountId: 1, status:1, date: -1})
  schema.index({ accountId: 1, _search:1})

  schema.plugin(transformPlugin({newIdName: 'id'}))
  return mongoose.model('Job', schema)
}
