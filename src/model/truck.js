// Truck data model

const {weightSchema, volumeSchema, transformPlugin} = require('@axioncore/acx-server-core').model

// Options
let options = {timestamps: true}

module.exports = ({mongoose}) => {

  // Schema
  let schema = mongoose.Schema({
    accountId: {type: String, required: true},
    plateNumber: String,
    name: String,
    make: String,
    model: String,
    year: Number,
    capacity: {
      weight: weightSchema,
      volume: volumeSchema,
    },
    active: {type: Boolean, required: true, default: true},
    _search: String,
  }, options)

  // Pre-save hook to populate some fields
  schema.pre('save', function(next) {
    this._search = this.plateNumber.toLowerCase()
    next()
  })

  // Indexes
  schema.index({accountId: 1, _search: 1, active: 1})

  schema.plugin(transformPlugin({newIdName: 'id'}))

  return mongoose.model('Truck', schema)
}
