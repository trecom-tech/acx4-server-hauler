const {transformPlugin} = require('@axioncore/acx-server-core').model

// Options
let options = {timestamps: true}

module.exports = ({mongoose}) => {

  // Schema
  let schema = mongoose.Schema({
    accountId: {type: String, required: true},
    userId: String,
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    fullName: String,
    _search: String,

    // Contacts
    email: String,
    phone: {type: String, required: true},

    license: {
      number: String
    },
    active: {type: Boolean, required: true, default: true}

  }, options)


  // Pre-save hook to populate some fields
  schema.pre('save', function(next) {
    this.fullName = this.firstName + ' ' + this.lastName
    this._search =
      (this.firstName + ' ' + this.lastName).toLowerCase() +
      ' ' + this.phone +
      (this.email ? ' ' + this.email : '')
    next()
  })

  // Indexes
  schema.index({phone:1})
  schema.index({accountId:1, phone:1})
  schema.index({accountId:1, userId:1})
  schema.index({accountId:1, _search:1, active: 1})

  schema.plugin(transformPlugin({newIdName: 'id'}))

  return mongoose.model('Driver', schema)
}
