const { profileSchema, transformPlugin } = require('@axioncore/acx-server-core').model

// Options
//let options = {timestamps: true}

module.exports = ({mongoose}) => {

  // Schema
  let schema = profileSchema//mongoose.Schema(profileSchema, options)

  // Indexes
  schema.index({accountId:1, name:1, active: 1})
  schema.plugin(transformPlugin({newIdName: 'id'}))
  return mongoose.model('Profile', schema)
}
