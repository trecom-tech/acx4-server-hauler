
// Create or update Hauler job based on the
// Logistic job
class LogisticJobProcessor {
  constructor({logger, config, job, profile, publisher, create_job}) {
    this.logger = logger
    this.config = config
    this.Job = job
    this.Profile = profile
    this.publisher = publisher
    this.createJob = create_job
  }

  // Job event represent Job data from Logistic module
  async process(context, jobEvent) {

    // If Hauler not assigned, ignore this event
    if(!jobEvent.carrier || !jobEvent.carrier.accountId) {
      return
    }

    this.logger.debug('Processing Logistic Job event', {action: 'notification', data: {jobId: jobEvent.id}})

    // Check if the corresponding Hauler Job already exists
    let job = await this.Job.findOne({
      accountId: jobEvent.carrier.accountId,
      'agent.jobId': jobEvent.id
    })

    // If job found allow changes only if it is in PENDING state
    if(job) {

      // Check for CANCELED job
      if(jobEvent.status == 'CANCELED' && ['PENDING', 'ASSIGNED', 'READY'].includes(job.status)) {
        job.status = 'CANCELED'
        await job.save()
        this.logger.debug('Job canceled by the customer', {
          action: 'logistic_job_event',
          data: {
            jobId: job.id
          }
        })

        // !!TODO: Publish notification? Including realtime?

      }
      else {
        this.logger.debug('Ignoring  logistic job change, updates are not supported', {
          action: 'logistic_job_event',
          data: {
            jobId: job.id,
            status: job.status
          }
        })
      }
      return
    }

    // Else create a new one

    // Prepare context
    let newContext = {
      action: 'create_job',
      accountId: jobEvent.carrier.accountId
    }

    // Get Agent/Shipper name from account profile
    let agent = await this.Profile.findOne({
      accountId: jobEvent.accountId
    })

    // Prepare data
    let data = {
      accountId: jobEvent.carrier.accountId,
      date: jobEvent.date,
      name: jobEvent.name,
      customer: {
        accountId: jobEvent.accountId,
        name: (agent ? agent.name : ''),
        job: {
          id: jobEvent.id,
          number: jobEvent.number,
        }
      },
      shipments: jobEvent.shipments,
      route: jobEvent.route,
      mapData: jobEvent.mapData
    }

    // Call Create Job action
    job = await this.createJob.execute(newContext, data)

    this.logger.debug('Run job is created', {
      action: 'logistic_job_event',
      data: {
        jobId: job.id
      }
    })

  }

}

module.exports = LogisticJobProcessor
