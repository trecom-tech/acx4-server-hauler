const { GetAction } = require('@axioncore/acx-server-core').actions

class GetDriver extends GetAction {
  constructor({config,logger,error,driver}) {
    super('get_driver', config, logger, error, driver)
  }
}

module.exports = GetDriver
