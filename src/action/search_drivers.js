const { SearchAction } = require('@axioncore/acx-server-core').actions

class SearchDrivers extends SearchAction {
  constructor({config, logger, error, driver}) {
    super('search_drivers', config, logger, error, driver, null, {fullName: 1}, null)
  }

}
module.exports = SearchDrivers
