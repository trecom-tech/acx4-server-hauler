const { GetAction } = require('@axioncore/acx-server-core').actions

class GetTruck extends GetAction {
  constructor({config, logger, error, truck}) {
    super('get_truck', config, logger, error, truck)
  }
}

module.exports = GetTruck
