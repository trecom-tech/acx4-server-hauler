const { SearchAction } = require('@axioncore/acx-server-core').actions

class SearchTrucks extends SearchAction {
  constructor({config, logger, error, truck}) {
    super('search_trucks', config, logger, error, truck, null, {plateNumber: 1}, null)
  }

}
module.exports = SearchTrucks
