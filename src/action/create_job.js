const { Action } = require('@axioncore/acx-server-core').actions

class CreateJob extends Action {
  constructor({
    config, logger, error, job, publisher
  }) {
    super('create_job', config, logger, error)
    this.Job = job
    this.publisher = publisher
  }

  async execute(context, data) {

    data.accountId = context.accountId

    let job = new this.Job(data)
    await job.save()

    //await this.publisher.publish(this.config.AWS_TOPIC_JOB, this.config.NOTIFICATION_RUN_JOB, context, job.toObject())
    return job
  }

}

module.exports = CreateJob
