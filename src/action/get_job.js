const { GetAction } = require('@axioncore/acx-server-core').actions

class GetJob extends GetAction {
  constructor({config,logger,error,job}) {
    super('get_job', config, logger, error, job)
  }
}

module.exports = GetJob
