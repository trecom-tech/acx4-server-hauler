const { DeleteAction } = require('@axioncore/acx-server-core').actions

class DeleteDriver extends DeleteAction {
  constructor({
    config, logger, error, driver, messages, api
  }) {
    super('delete_driver', config, logger, error, messages, driver)
    this.api = api
  }

  async execute(context, data) {

    // Check if exists
    let existing = await this.Model.findOne({
      accountId: context.accountId,
      _id: data.id
    })

    if(!existing) {
      throw new this.error.create(404, 'not_found')
    }

    // Delete user
    await this.api.call(
      'delete',
      '/account/users/' + existing.userId,
      'delete_user',
      context,
      {
        userId: existing.userId
      }
    )

    // Delete driver
    await super.execute(context, data)

    return this.messages.withCode(200, 'ok')

  }

}

module.exports = DeleteDriver
