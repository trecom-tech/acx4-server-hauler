const {UpdateAction} = require('@axioncore/acx-server-core').actions

class UpdateJob extends UpdateAction {
  constructor({config, logger, error, job}) {
    super('update_job', config, logger, error, job, [])
  }

  async execute(context, data, query) {

    // We allow to update only
    // - truck
    // - driver
    // - status

    let updateData = {}

    if(data.truck) {
      updateData.truck = data.truck
    }

    if(data.driver) {
      updateData.driver = data.driver
    }

    if(data.status) {
      updateData.status = data.status
    }

    updateData.accountId = context.accountId
    updateData.id = data.id

    return await super.execute(context, updateData, query)
  }

}

module.exports = UpdateJob
