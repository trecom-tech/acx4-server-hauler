const { SearchAction } = require('@axioncore/acx-server-core').actions

class SearchJobs extends SearchAction {
  constructor({config, logger, error, job, driver}) {
    super('search_jobs', config, logger, error, job, null, {date: -1}, null)
    this.Driver = driver
  }

  async execute(context, data, query) {

    // Check if a driver user
    // Driver can access only own jobs

    // TODO:  Current validation is based on checking if
    // Driver records exists for the given userId
    // in the future we want to fix it by using Roles and Permissions
    let driver = await this.Driver.findOne({
      accountId: context.accountId,
      userId: context.userId
    })

    if(driver) {
      data['driver.id'] = driver.id
    }

    return await super.execute(context, data, query)
  }
}
module.exports = SearchJobs
