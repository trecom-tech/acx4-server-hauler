const { Action } = require('@axioncore/acx-server-core').actions
const _ = require('lodash')

class CreateDriver extends Action {
  constructor({
    config, logger, error, driver, publisher, api
  }) {
    super('create_driver', config, logger, error)
    this.Driver = driver
    this.publisher = publisher
    this.api = api
  }

  async execute(context, data) {

    data.accountId = context.accountId

    // Check if driver with the phone exists
    let existing = await this.Driver.findOne({phone: data.phone})
    if(existing) {
      this.logger.debug('Found existing driver with phone', {
        action: 'create_driver',
        data: existing.toObject()
      })

      throw this.error.create(400, 'data_exists')
    }

    if(!data.password) {
      throw this.error.create(400, 'password_required')
    }

    // Create user account first
    let user = await this.api.call(
      'post', '/account/users', 'create_user', context,
      {
        userName: data.phone,
        firstName: data.firstName,
        lastName: data.lastName,
        contacts: [{
          type: 'PHONE',
          label: 'Cell',
          id: data.phone
        }],
        password: data.password
      }
    )

    this.logger.debug('Driver User created', {
      acction: 'create_driver',
      data: {userId: user.id}
    })

    let driver = new this.Driver(_.assign(data, {userId: user.id}))
    await driver.save()

    //await this.publisher.publish(this.config.AWS_TOPIC_CUSTOMER, this.config.NOTIFICATION_CUSTOMER, context, customer)
    return driver
  }

}

module.exports = CreateDriver
