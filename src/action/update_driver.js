const {UpdateAction} = require('@axioncore/acx-server-core').actions

class UpdateDriver extends UpdateAction {
  constructor({config, logger, error, driver}) {
    super('update_driver', config, logger, error, driver, [])
  }

}

module.exports = UpdateDriver
