const {UpdateAction} = require('@axioncore/acx-server-core').actions

class UpdateTruck extends UpdateAction {
  constructor({config, logger, error, truck}) {
    super('update_truck', config, logger, error, truck, [])
  }

}

module.exports = UpdateTruck
