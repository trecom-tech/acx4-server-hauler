const { DeleteAction } = require('@axioncore/acx-server-core').actions

class DeleteTruck extends DeleteAction {
  constructor({
                config, logger, error, truck, messages
              }) {
    super('delete_carrier', config, logger, error, messages, truck, 'active', false)
  }

}

module.exports = DeleteTruck
