const { Action } = require('@axioncore/acx-server-core').actions

class CreateTruck extends Action {
  constructor({ config, logger, error, truck, publisher }) {
    super('create_truck', config, logger, error)
    this.Truck = truck
    this.publisher = publisher
  }

  async execute(context, data) {

    data.accountId = context.accountId

    let truck = new this.Truck(data)
    await truck.save()

    return truck
  }

}

module.exports = CreateTruck
