const awilix = require('awilix')
const {Application, main, events} = require('@axioncore/acx-server-core')
const config = require('./config')
const messages = require('./config/message')

// Account module application
class App extends Application {

  constructor() {
    console.log('Creating Hauler App')
    super(config)
  }

  // Initialize application
  async init() {

    // Init Super class for core components
    await super.init()

    // Load application modules
    super.getContainer().loadModules([
      'action/*.js',
      'model/*.js',
      'event/*.js',
      'service/*.js'
    ], {
      cwd: __dirname,
      resolverOptions: {
        lifetime: awilix.Lifetime.SINGLETON
      }
    })

    // Add simple Notification processor from core
    super.getContainer().register({
      messages: awilix.asClass(messages).singleton(),
      account_event: awilix.asClass(events.AccountEventProcessor).singleton()
    })
  }

  // Start application
  async start() {
    await super.start()
  }

  // Stop application
  async stop() {
    await super.stop()
  }

}

// Execute main function
main(new App())
