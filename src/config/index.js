// Default settings for all environments
const config = {
  // Module name
  MODULE_NAME: 'Hauler',
  // Module port
  MODULE_PORT: 8040,
  // AWS deployment region
  AWS_REGION: 'us-east-2',
  // DB name
  MONGO_DB_NAME: 'acx-hauler',
  // MONGOOSE CONNECTION POOL SIZE
  MONGO_DB_POOL_SIZE: 10,
  // Enable Log to console
  LOG_CONSOLE: false,
  // Log file
  LOG_FILE: '../log/acx-server-hauler.log',
  // Log level
  LOG_LEVEL: 'info',
  // Incoming notification types
  NOTIFICATION_JOB_REQUEST:  'job_request_event',
  // Outgoing notifications
  NOTIFICATION_RUN_JOB: 'run_job',
  NOTIFICATION_RUN_JOB_STATUS: 'run_job_status',

  // Max Page size
  MAX_PAGE_SIZE: 100,
  // Default Page size
  DEFAULT_PAGE_SIZE: 100

}

// Environment specific configuration
switch(process.env.NODE_ENV) {

// Production environment
case 'production':
  // Encryption Key
  config.AWS_KMS_KEY_ID = '9e21bce5-d346-47a3-a81c-d33d93131c18',
  // AWS Notification queue to listen notifications
  config.AWS_NOTIFICATION_QUEUE = 'https://sqs.us-east-2.amazonaws.com/125538167405/prod-acx-hauler-notification'
  // AWS Topic to piblish Carrier Job notifications
  config.AWS_TOPIC_JOB = 'arn:aws:sns:us-east-2:125538167405:prod-acx-hauler-job'

  // Mongo DB connection URL
  // For production it is expected that env. variable ACX_DB_URL will be set
  config.MONGO_DB_URL = process.env.ACX_DB_URL
  // DB name
  config.MONGO_DB_NAME = 'prod-acx-hauler'
  break

// AWS development environment
case 'development':
  // Encryption Key
  config.AWS_KMS_KEY_ID = '9e21bce5-d346-47a3-a81c-d33d93131c18'
  // AWS Notification queue to listen notifications
  config.AWS_NOTIFICATION_QUEUE = 'https://sqs.us-east-2.amazonaws.com/125538167405/dev-acx-hauler-notification'
  // AWS Topic to piblish Warehouse Job notifications
  config.AWS_TOPIC_JOB = 'arn:aws:sns:us-east-2:125538167405:dev-acx-hauler-job'

  // Mongo DB connection URL
  config.MONGO_DB_URL = 'mongodb+srv://developer:aXioncoreDev2018@cluster0-fgl45.mongodb.net/'
  // DB name
  config.MONGO_DB_NAME = 'dev-acx-hauler'
  // Override Log level
  config.LOG_LEVEL = 'debug'

  // API Base URL
  config.API_BASE_URL = 'https://dev-api.vasanda.com'
  config.API_SEND_CONTEXT = false

  break

// Default is configuration for LOCAL development
default:
  // Encryption Key
  config.AWS_KMS_KEY_ID = '9e21bce5-d346-47a3-a81c-d33d93131c18'
  // AWS Notification queue to listen notifications
  config.AWS_NOTIFICATION_QUEUE = 'https://sqs.us-east-2.amazonaws.com/125538167405/local-acx-hauler-notification'
  // AWS Topic to piblish Warehouse Job notifications
  config.AWS_TOPIC_JOB = 'arn:aws:sns:us-east-2:125538167405:local-acx-hauler-job'

  // Mongo DB connection URL
  config.MONGO_DB_URL = 'mongodb://localhost/acx-hauler'
  // Enable Log to console
  config.LOG_CONSOLE = true
  // Override Log level
  config.LOG_LEVEL = 'debug'

  // API Base URL
  config.API_BASE_URL = 'http://localhost'
  config.API_SEND_CONTEXT = true

  break
}

module.exports = config
