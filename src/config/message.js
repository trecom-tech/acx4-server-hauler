const { Messages } = require('@axioncore/acx-server-core')

const messages = {

  some_message: {
    en: 'Some message'
  }

}

// Extends default Message and add additional messages
// for Hauler module
class HaulerMessages extends Messages {
  constructor( { config } ) {
    super({config: config})
    Object.assign(this.messages, messages)
  }
}

module.exports = HaulerMessages
