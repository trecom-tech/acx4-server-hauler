#!/bin/bash
source /opt/acx/environment.sh
cd /opt/acx/acx-server-hauler
pm2 stop --silent hauler
pm2 delete --silent hauler
